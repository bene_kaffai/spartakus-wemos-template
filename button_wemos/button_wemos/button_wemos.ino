#include <Arduino.h>

// MQTT from https://github.com/256dpi/arduino-mqtt
// !!!! change MQTT Buffersize to 256 in MQTTClient.h !!!!

#include <ESP8266WiFi.h>
#include <MQTTClient.h>

const char *ssid = "CherrysNest";
const char *pass = "30012407nelio10W";
const char *broker = "192.168.2.136"; // "localhost"; "broker.hivemq.com";

WiFiClient net;
MQTTClient mqtt;


unsigned long lastMillis = 0;

const char *CLIENTID = "WEMOS_TASTER_1";
const char *NETWORK = "/spartakus/thing/";
const String TOPIC_OUT = "/spartakus/thing/" + String(CLIENTID) + "/output/";
const String TOPIC_IN = "/spartakus/thing/" + String(CLIENTID) + "/input/";
const String TOPIC_PING = "/spartakus/thing/all/input/ping";

String report =
  "{\"name\":\"WEMOS_TASTER_1\",\"status\":1,\"type\":\"hardware\",\"input\":{\"report\":1,\"SWITCH\":1},\"output\":{\"report\":1,\"SWITCH\":\"STRING\"}}";

//Button
const byte tasterPin = D1; // Pin fuer Taster
const byte ledPin = D2; // Pin fuer Taster

int buttonState = 0;
int statechange = 0;
boolean CHANGED = false;


void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!mqtt.connect(CLIENTID)) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
  mqtt.subscribe(TOPIC_IN+"#");
  mqtt.publish(TOPIC_OUT + "report", report);
  Serial.println("mqtt subscribe, published IO report");
}

void setup() {
  Serial.begin(9600);

  // initialize the LED pin as an output:
  pinMode(ledPin, OUTPUT);

  // initialize the pushbutton pin as an input:
  pinMode(tasterPin, INPUT);

  // CONNECT TO SPARTANET WLAN //
  WiFi.begin(ssid, pass);

  // CONNECT TO LOCAL MQTT BROKER //
  mqtt.begin(broker, net);
  connect();
}

void loop() {
  mqtt.loop();
  delay(10); // <- fixes some issues with WiFi stability

  if (!mqtt.connected()) {
    connect();
  }

  // Taster

  buttonState = digitalRead(tasterPin);
    if (statechange != buttonState){
      CHANGED = true;
    } else {
      CHANGED = false;
    }
    // check if the pushbutton is pressed.
    // if it is, the buttonState is HIGH:
    if (buttonState == HIGH && CHANGED) {
        // turn LED on:
        digitalWrite(ledPin, HIGH);
        mqtt.publish(TOPIC_OUT + "SWITCH", "PRESSED");
        Serial.print(TOPIC_OUT + "SWITCH" + " PRESSED\n");
        statechange = buttonState;
    }
    else if(CHANGED) {
        // turn LED off:
        digitalWrite(ledPin, LOW);
        mqtt.publish(TOPIC_OUT + "SWITCH", "RELEASED");
        Serial.print(TOPIC_OUT + "SWITCH" + " RELEASED\n");
        statechange = buttonState;
    }

  // publish a message roughly every half second.
  if (millis() - lastMillis > 5000) {
    lastMillis = millis();
    //mqtt.publish(TOPIC_IN + "test", "1");
    Serial.println("example timer");
  }
}

void messageReceived(String topic, String payload, char * bytes, unsigned int length) {
// Standard
  Serial.print("incoming: ");
  Serial.print(topic);
  Serial.print(" - ");
  Serial.print(payload);
  Serial.println();
  if (topic.equalsIgnoreCase(TOPIC_IN+"SWITCH")) {
    ledcontrol(payload);
  }
}

void ledcontrol(String message) {
  //Turn ON LED
  if(message.equalsIgnoreCase("PRESSED")){
    digitalWrite(ledPin, HIGH);
    mqtt.publish(TOPIC_OUT + "SWITCH", "PRESSED");
    Serial.println("EXTERN PRESS");
  } else {
    // turn LED off:
    digitalWrite(ledPin, LOW);
    mqtt.publish(TOPIC_OUT + "SWITCH", "RELEASED");
    Serial.println("EXTERN RELEASE");
  }
}
