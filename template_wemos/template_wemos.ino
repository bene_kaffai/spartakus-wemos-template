// MQTT from https://github.com/256dpi/arduino-mqtt
// !!!! change MQTT Buffersize to 256 in MQTTClient.h !!!!

#include <ESP8266WiFi.h>
#include <MQTTClient.h>

const char *ssid = "Kabellos";
const char *pass = "criXus72";
const char *broker = "broker.hivemq.com"; // "localhost";

WiFiClient net;
MQTTClient mqtt;


unsigned long lastMillis = 0;

const char *CLIENTID = "WEMOS_TEMPLATE";
const char *NETWORK = "/spartakus/thing/";
const String TOPIC_OUT = "/spartakus/thing/" + String(CLIENTID) + "/output/";
const String TOPIC_IN = "/spartakus/thing/" + String(CLIENTID) + "/input/";
const String TOPIC_PING = "/spartakus/thing/all/input/ping";

String report =
  "{\"name\":\"WEMOS_TEMPLATE\",\"status\":1,\"type\":\"hardware\",\"input\":{\"report\":1,\"INPUT_DUMMY\":1},\"output\":{\"report\":1,\"OUTPUT_DUMMY\":\"TYPE_OF_OUTPUT\"}}";

void connect() {
  Serial.print("checking wifi...");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(1000);
  }

  Serial.print("\nconnecting...");
  while (!mqtt.connect(CLIENTID)) {
    Serial.print(".");
    delay(1000);
  }

  Serial.println("\nconnected!");
  mqtt.subscribe(TOPIC_IN+"#");
  mqtt.subscribe(TOPIC_PING);
  mqtt.publish(TOPIC_OUT + "report", report);
  Serial.println("mqtt subscribe, published IO report");
}

void setup() {
  Serial.begin(9600);

  // CONNECT TO SPARTANET WLAN //
  WiFi.begin(ssid, pass);

  // CONNECT TO LOCAL MQTT BROKER //
  mqtt.begin(broker, net);
  connect();
}

void loop() {
  mqtt.loop();
  delay(10); // <- fixes some issues with WiFi stability

  if (!mqtt.connected()) {
    connect();
  }

  // publish a message roughly every half second.
  if (millis() - lastMillis > 5000) {
    lastMillis = millis();
    //mqtt.publish(TOPIC_IN + "test", "1");
    Serial.println("example timer");
  }
}

void messageReceived(String topic, String payload, char * bytes, unsigned int length) {
  Serial.print("incoming: ");
  Serial.print(topic);
  Serial.print(" - ");
  Serial.print(payload);
  Serial.println();
  if (topic.equalsIgnoreCase(TOPIC_IN+"report")) {
      mqtt.publish(TOPIC_OUT + "report", report);
  }
  else if (topic.equalsIgnoreCase(TOPIC_PING)) {
      mqtt.publish(TOPIC_OUT + "report", report);
}
